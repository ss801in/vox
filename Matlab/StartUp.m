BaseDir      = ['c:/Users/Sudarshan/Work/VOX/'];
DataDir      = [BaseDir, 'Data/'];
MatLabDir    = [BaseDir, 'Matlab/'];
MatUtilsDir  = [MatLabDir, 'Utils/'];
MatScriptDir = [MatLabDir, 'Scripts/'];

addpath(MatUtilsDir);
addpath(MatScriptDir);
