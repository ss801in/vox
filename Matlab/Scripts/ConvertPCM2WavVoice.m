%
% ConvertPCMToWav:
%   This script converts .pcm files kept in <Data> to .wav files.
%   This script is mostly for test purposes.
%

DefineGlobalVariables;

InputDir  = [DataDir, 'Voice/PCM/'];
OutputDir = [DataDir, 'Voice/WavOut8K/']; 

% read the file
FileNames   = ['sm1';
	       'sm2';
	       'sm3';
	       'sm4'];

[NumFiles,SizeNames]   = size(FileNames);

for i = 1:NumFiles
  InputFile  = [InputDir,FileNames(i,:),'.pcm'];
  OutputFile = [OutputDir,FileNames(i,:),'.wav'];

  disp(['Converting ', InputFile, '...']);
  errCode    = pcm2wav(InputFile,OutputFile);
end %for

