%
% ConvertWavToPCM:
%   This script converts .wav files kept in <Data> to raw 8-bit PCM format.
%

DefineGlobalVariables;

InputDir  = [DataDir, 'Voice/Wav8K/'];
OutputDir = [DataDir, 'Voice/PCM/']; 

% read the file
FileNames   = ['sm1';
	       'sm2';
	       'sm3';
	       'sm4';
	       'sm5';
	       'sm6'];

[NumFiles,SizeNames]   = size(FileNames);

for i = 1:NumFiles
  InputFile  = [InputDir,FileNames(i,:),'.wav'];
  OutputFile = [OutputDir,FileNames(i,:),'.pcm'];

  disp(['Converting ', InputFile, '...']);
  errCode    = wav2pcm(InputFile,OutputFile);
end %for


