%
% FindPSDPeaks:
%   finds peaks in a PSD of a signal and displays it. 
% 
%

clear all;
DefineGlobalVariables;

%inDir     = [DataDir, 'NarrowBand/PCM/'];
%outDir    = [DataDir, 'NarrowBand/WavOut8K/'];
%fileName  = 'tm1.pcm';
%outName   = 'tm1x.wav';

inDir      = [DataDir, 'Voice/PCM/'];
outDir     = [DataDir, 'Voice/WavOut8K/'];
fileName   = 'sm3.pcm';
outName    = 'sm3x.wav';

inFile     = [inDir,fileName];

fid        = fopen(inFile,'r');
iSig       = fscanf(fid,'%d');
fclose(fid);

fSig       = (iSig - 127)/128;

N          = 256;
density    = powSpDen(fSig,N);

[K,N]      = size(density);

%uFilt        = [0.25,0.25,0.25,0.25];
uFilt       = [1.0,0.0,0.0,0.0];
noiseThr     = 60.0/N;
maxNumPeaks  = 10;

first   = 1;
last    = N;
outSig  = [];
zSig    = zeros(1,N);
maxSigPow = 0;
for i   = 1:K
  x        = fSig(first:last)';
  fDensity = conv(density(i,:),uFilt,'same');  
  sigPow   = x*x';

  if(maxSigPow < sigPow)
    maxSigPow = sigPow;
  end %if

  if(sigPow > noiseThr*N)
    % Find Peaks
    [peaks,peakCount] = findPeaks(density(i,:),noiseThr,maxNumPeaks);
    % Display peaks
    displayPeaks(density(i,:),peaks,peakCount);
  end %if

  first = last + 1;
  last  = first + N - 1;
end %for

disp(['maximum signal power:', num2str(maxSigPow)]);

	    
