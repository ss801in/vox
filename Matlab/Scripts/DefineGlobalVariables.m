%
% DefineGlobalVaribles:
%   This script defines global variables
%
%

BaseDir      = ['c:/Users/Sudarshan/Work/VOX/'];
DataDir      = [BaseDir, 'Data/'];
MatLabDir    = [BaseDir, 'Matlab/'];
MatUtilsDir  = [MatLabDir, 'Utils/'];
MatScriptDir = [MatLabDir, 'Scripts/'];
