%
% ConvertPCMToWav:
%   This script converts .pcm files kept in <Data> to .wav files.
%   This script is mostly for test purposes.
%

DefineGlobalVariables;

InputDir  = [DataDir, 'NarrowBand/PCM/'];
OutputDir = [DataDir, 'NarrowBand/WavOut8K/']; 

% read the file
FileNames   = ['tm1';
	       'tm2';
	       'tm7'];

[NumFiles,SizeNames]   = size(FileNames);

for i = 1:NumFiles
  InputFile  = [InputDir,FileNames(i,:),'.pcm'];
  OutputFile = [OutputDir,FileNames(i,:),'.wav'];

  disp(['Converting ', InputFile, '...']);
  errCode    = pcm2wav(InputFile,OutputFile);
end %for

FileNames   = ['tm13';
	       'tm14';
	       'tm18';
	       'tm19';
	       'tm20';
	       'tm28';
	       'tm30';
	       'tm31';
	       'tm32'];

[NumFiles,SizeNames]   = size(FileNames);

for i = 1:NumFiles
  InputFile  = [InputDir,FileNames(i,:),'.pcm'];
  OutputFile = [OutputDir,FileNames(i,:),'.wav'];

  disp(['Converting ', InputFile, '...']);
  errCode    = pcm2wav(InputFile,OutputFile);
end %for
