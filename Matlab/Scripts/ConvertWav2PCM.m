%
% ConvertWavToPCM:
%   This script converts .wav files kept in <Data> to raw 8-bit PCM format.
%

DefineGlobalVariables;

InputDir  = [DataDir, 'NarrowBand/Wav8K/'];
OutputDir = [DataDir, 'NarrowBand/PCM/']; 

% read the file
FileNames   = ['tm1';
	       'tm2';
	       'tm7'];

[NumFiles,SizeNames]   = size(FileNames);

for i = 1:NumFiles
  InputFile  = [InputDir,FileNames(i,:),'.wav'];
  OutputFile = [OutputDir,FileNames(i,:),'.pcm'];

  disp(['Converting ', InputFile, '...']);
  errCode    = wav2pcm(InputFile,OutputFile);
end %for

FileNames   = ['tm13';
	       'tm14';
	       'tm18';
	       'tm19';
	       'tm20';
	       'tm28';
	       'tm30';
	       'tm31';
	       'tm32'];

[NumFiles,SizeNames]   = size(FileNames);

for i = 1:NumFiles
  InputFile  = [InputDir,FileNames(i,:),'.wav'];
  OutputFile = [OutputDir,FileNames(i,:),'.pcm'];

  disp(['Converting ', InputFile, '...']);
  errCode    = wav2pcm(InputFile,OutputFile);
end %for

