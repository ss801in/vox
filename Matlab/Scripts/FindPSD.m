%
% FindPSD:
%   finds PSD of a signal and displays it. 
% 
%

clear all;
DefineGlobalVariables;

%inDir     = [DataDir, 'NarrowBand/PCM/'];
%outDir    = [DataDir, 'NarrowBand/WavOut8K/'];
%fileName  = 'tm2.pcm';
%outName   = 'tm2x.wav';

inDir      = [DataDir, 'Voice/PCM/'];
outDir     = [DataDir, 'Voice/WavOut8K/'];
fileName   = 'sm5.pcm';
outName    = 'sm5x.wav';

inFile     = [inDir,fileName];

fid        = fopen(inFile,'r');
iSig       = fscanf(fid,'%d');
fclose(fid);

fSig       = (iSig - 127)/128;

N          = 1024;
density    = powSpDen(fSig,N);

[K,N]      = size(density);

%uFilt      = [0.25,0.25,0.25,0.25];
uFilt      = [1.0,0.0,0.0,0.0];
nThr       = 0.0;

first   = 1;
last    = N;
outSig  = [];
zSig    = zeros(1,N);
maxSigPow = 0;
for i   = 1:K
  x        = fSig(first:last)';
  fDensity = conv(density(i,:),uFilt,'same');  
  sigPow   = sum(fDensity);
  if(maxSigPow < sigPow)
    maxSigPow = sigPow;
  end %if  
  %if(sigPow > nThr)
    outSig = [outSig,x];  	    
    %disp(['signal power = ', num2str(sigPow)]);
    %disp(outSig);
    %plot(fDensity);
    %pause;
  %else
  %  outSig = [outSig,zSig];
  %end %if
  %disp(outSig);
  %pause;
  first = last + 1;
  last  = first + N - 1;
end %for

disp(['maximum signal power:', num2str(maxSigPow)]);

outFile     = [outDir,outName];

fs   = 8000;
bits = 8;

wavwrite(outSig,fs,bits,outFile);
	    
