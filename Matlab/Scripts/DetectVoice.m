%
% FindPSDPeaks:
%   finds peaks in a PSD of a signal and displays it. 
% 
%

clear all;
DefineGlobalVariables;

%inDir      = [DataDir, 'NarrowBand/PCM/'];
%outDir     = [DataDir, 'NarrowBand/WavOut8K/'];
%fileName   = 'tm1.pcm';
%outPCMName = 'tm1x.pcm';
%outName    = 'tm1x.wav';

inDir      = [DataDir, 'Voice/PCM/'];
outDir     = [DataDir, 'Voice/WavOut8K/'];
fileName   = 'sm3.pcm';
outPCMName = 'sm3x.pcm';
outName    = 'sm3x.wav';

inFile     = [inDir,fileName];

fid        = fopen(inFile,'r');
iSig       = fscanf(fid,'%d');
fclose(fid);

fSig       = (iSig - 127)/128;

N          = 256;
density    = powSpDen(fSig,N);

[K,N]      = size(density);

uFilt        = [0.25,0.25,0.25,0.25];
%uFilt       = [1.0,0.0,0.0,0.0];
noiseThr     = 40.0/N;
maxNumPeaks  = 10;

first   = 1;
last    = N;
fOutSig = [];
zSig    = zeros(1,N);
maxSigPow = 0;
for i   = 1:K
  x        = fSig(first:last)';
  fDensity = conv(density(i,:),uFilt,'same');  
  sigPow   = x*x';

  if(maxSigPow < sigPow)
    maxSigPow = sigPow;
  end %if

  if(sigPow > noiseThr*N)
    % Find Peaks
    [peaks,peakCount] = findPeaks(density(i,:),noiseThr,maxNumPeaks);

    % decide if it is voice
    if(peakCount >= 4)
      fOutSig = [fOutSig,x];
    else
      fOutSig = [fOutSig,zSig];
    end %if
  end %if

  first = last + 1;
  last  = first + N - 1;
end %for

disp(['maximum signal power:', num2str(maxSigPow)]);

% Write the output
OutSig =  fOutSig*128 + 127;

outPCMFile = [outDir,outPCMName];
fid        = fopen(outPCMFile,'w');
iSig       = fprintf(fid,'%d\n',OutSig);
fclose(fid);

outFile = [outDir,outName];
pcm2wav(outPCMFile,outFile);

%fs      = 8000;
%bits    = 8;
%wavwrite(OutSig,fs,bits,outFile);
