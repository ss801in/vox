%
% function pcm2wav:
%    converts an 8-bit pcm array given in <InFile> to a wav file and puts
%    it in <outFile>.
%    The function assumes that the pcm array is 8-bit(range 0 to 255), and
%    sampled at 8KHz.  
%
%
function [errCode] = pcm2wav(inFile, outFile)
  errCode = 0;

  fid = fopen(inFile,'r');
  ix  = fscanf(fid,'%d');
  fclose(fid);

  x    = (ix - 127)/128;
  fs   = 8000;
  bits = 8;

  wavwrite(x,fs,bits,outFile);

end %function
