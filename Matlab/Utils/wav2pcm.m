%
% function pcm2wav:
%    converts an 8-bit pcm array given in <InFile> to a wav file and puts
%    it in <outFile>.
%    The function assumes that the pcm array is 8-bit(range 0 to 255), and
%    sampled at 8KHz.  
%
%
function [errCode] = wav2pcm(inFile, outFile)
  errCode = 0;

  [rawx,fs,nbits] = wavread(inFile);
  x               = uint8(rawx*128 + 127);


  fid = fopen(outFile,'w');
  fprintf(fid,'%d \n',x);
  fclose(fid);

end %function
