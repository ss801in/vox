%
% findPeaks:
%   given a fraction <maxPeakFraction> and maximum number <maxNumPeaks>,
%   this function
%   finds <numPeaks> peaks that contain a fraction <peakFraction> of total
%   energy.
%   It keeps on finding peaks till the cumulative peak energy reaches the
%   fraction <maxPeakFraction>, or number of peaks reach <maxNumPeaks>.
%   In case peak energy reaches <maxPeakFraction> first, <numPeaks> gives
%   number of peaks in which this fraction of energy is contained.
%   If <numPeaks> reach <maxNumPeaks> first, <peakFraction> is the fraction
%   of energy contained in these peaks.
%   <noiseThr> gives the noise threshold.
%
%

function [peaks, peakCount, errCode] = findPeaks(sigPSD,noiseThr,maxNumPeaks)
  errCode      = 0;

  sigLen       = length(sigPSD);
  peaks        = zeros(maxNumPeaks, 2);

  halfLen      = sigLen/2;
  halfPSD      = sigPSD(1:halfLen);

  peakCount     = 0;

  foundAllPeaks = 0;
  while(foundAllPeaks == 0)
    [maxPSD,maxIndex] = max(halfPSD);
    % Move left 
    leftIndex = maxIndex;
    moveLeft  = 1; 
    while(moveLeft == 1)
      if(halfPSD(leftIndex) >= noiseThr)
        halfPSD(leftIndex) = 0;  
        leftIndex = leftIndex - 1;
      else
        moveLeft  = 0;
        leftIndex = leftIndex + 1;
        if(leftIndex > halfLen)
          leftIndex = halfLen
        end %if
      end %if
      if(leftIndex <= 1)
        leftIndex = 1;
        moveLeft  = 0;
      end %if
    end %while

    % Move right
    if(maxIndex < halfLen)
      rightIndex = maxIndex +1;
    else
      rightIndex = maxIndex;    
    end %if
    
    moveRight  = 1; 
    while(moveRight == 1)
      if(halfPSD(rightIndex) >= noiseThr)
        halfPSD(rightIndex) = 0;
        rightIndex          = rightIndex + 1;		      
      else
        moveRight           = 0;
        rightIndex          = rightIndex - 1;
        if(rightIndex < 1)
          rightIndex = 1;
        end %if	
      end %if

      if(rightIndex >= halfLen)
        rightIndex = halfLen;
        moveRight  = 0;
      end %if
    end %while

    if(rightIndex > leftIndex)
      peakCount = peakCount +1;
      peaks(peakCount,1) = leftIndex;
      peaks(peakCount,2) = rightIndex;
      if(peakCount >= maxNumPeaks)
        foundAllPeaks = 1;
      end %if
    else
      foundAllPeaks = 1;
    end %if
  end %while

end %function
