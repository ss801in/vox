%
% stft:
%   gives <N> point short time fourier transform of the signal <sig>
%
%
function [outStft, errCode] = stft(sig, N)

  errCode = 0;

  % find number of fft to be taken 
  sigLen  = length(sig);
  K       = floor(sigLen/N);

  % initialize psd matrix
  outStft = zeros(K,N);

  % find psd
  first = 1;
  last  = N;
  for i = 1:K
     x             = sig(first:last);
     fftX          = fft(x);
     outStft(i,:)  = fftX/N;

     first         = last  + 1;
     last          = first + N - 1;
  end %for
end %function
