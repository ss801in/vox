%
% DisplayPeaks
%

function errCode = displayPeaks(sigPSD, peaks, peakCount)
	 
  errCode = 0;

  sigLen       = length(sigPSD);
  halfLen      = sigLen/2;
  halfPSD      = sigPSD(1:halfLen);

  [maxPSD,maxIndex] = max(halfPSD);

  peakPlot    = zeros(1,halfLen);
  for peakIndex = 1:peakCount
    leftIndex  = peaks(peakIndex,1);
    rightIndex = peaks(peakIndex,2);

    peakPlot(leftIndex:rightIndex) = ones(1,rightIndex -leftIndex +1);
  end %for
  peakPlot = maxPSD*peakPlot;
  close all;
  figure;
  plot(halfPSD,'b-');
  hold on;
  plot(peakPlot,'r--');
  grid on;
  pause;
end %function 
