%
% powSpDen:
%   gives <N> point windowed power spectral density of the signal <sig>
%
%
function [density, errCode] = powSpDen(sig, N)

  errCode = 0;

  % find number of fft to be taken 
  sigLen  = length(sig);
  K       = floor(sigLen/N);

  % initialize psd matrix
  density = zeros(K,N);

  % find psd
  first = 1;
  last  = N;
  for i = 1:K
     x             = sig(first:last);
     fftX          = fft(x);
     fftXT         = transpose(fftX);
     density(i,:)  = (fftX.*fftXT')/N;

     %sigpow        = x'*x;
     %density(i,:)  = density(i,:)/sigpow;

     fftpow        = sum(density(i,:));
     
     % DEBUG
     %disp(['sigpow =', num2str(sigpow),'fftpow =',num2str(fftpow)]);
     %plot(density(i,:));
     %pause;
     % DEBUG
     
     first         = last  + 1;
     last          = first + N - 1;
  end %for
end %function
